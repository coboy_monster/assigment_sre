import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AssignmentSRE {
    private static final Scanner scanner = new Scanner(System.in);
    public static void main(String [] args) {
        String[] aItems = null;
        if(args.length == 0){
            System.out.print("Please Enter Your number of Resource : ");
             scanner.nextLine().split(" ");
        }else{
            aItems=args;
        }
        int input_ = Integer.parseInt(aItems[0]);
        boolean isT1 = true;
        List<Integer> t1 = new ArrayList<>();
        List<Integer> t2 = new ArrayList<>();

        for (int i = 1; i < input_ + 1; i++) {
            if (isT1) {
                t1.add(i);
                isT1 = false;
            } else {
                t2.add(i);
                isT1 = true;
            }
        }
        System.out.println("T1 = " + t1.toString());
        System.out.println("T2 = " + t2.toString());
    }
}

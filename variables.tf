variable "access_key" {
  type = "string"
  default = ""
}
variable "secret_key" {
  type = "string"
  default = ""
}
variable "region" {
  type = "string"
  default = "ap-southeast-5"
}
variable "name" {
  type = "string"
  default = "sre-instance"
}
variable "password" {
  type = "string"
  default = "Test1234!"
}

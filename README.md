# Assigment_SRE

## Question : Create account in alibaba cloud indonesia, use free tier credit    
    a. Create terraform project code that store in repo gitlab/github -> make sure the project can be access by public
    b. Create ECS instance that can auto scale, up to 2 server
    c. The server must use Indonesian Timezone, docker installed, run wordpress on top of docker
    d. Wordpress should be accessible from internet
    e. Ssh or telnet not allowed from internet

## How To Run 
```
terraform init
terraform plan
terraform apply

make sure your user version of terraform :
Terraform v0.12.15
+ provider.alicloud v1.121.2
```


## Question : Solve with any programming language
    a. Terdapat pool resource dg number of resource n ( ex 10 resources = 1,2,3,4,5,6,7,8,9,10)
    b. Ada 2 team lead yg memiliki preferensi resource (preferensi di input oleh masing-masing team lead, dimulai dari T1 dan diikuti oleh T2)
    T1 = 3,5,1,4,2
    T2 = 1,2,7,8,9
    c. Kedua team lead memiliki kesempatan untuk mengambil resources yang ada secara berurutan (round-robin), seimbang dan bergantian, sehingga resource yg akan didapat:
    T1 = 3,5,4,6,9
    T2 = 1,2,7,8,10
    note -> jika resource yang dipilih sudah diambil oleh team lead lain, maka sistem akan
    otomatis memberikan available resource dari yang terkecil
    goal -> print out member dari tiap team lead yg berhasil di pilih.

## How To Run 
```
javac AssignmentSRE.java

java AssignmentSRE $addYourNumberOfReSource
    or
java AssignmentSRE
Please Enter Your number of Resource : $addYourNumberOfReSource
```
